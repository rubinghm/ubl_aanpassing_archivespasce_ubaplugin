class ApAccessionReport < AbstractReport
  register_report

  def fix_row(row)
    clean_row(row)
    row[:linked_resources] = AccessionResourcesSubreport.new(self, row[:id]).get_content
    row.delete(:id)
  end

  def query
    results = db.fetch(query_string)
    results_new = db.fetch(query_string_new)
    results_in_progress = db.fetch(query_string_in_progress)
    results_completed = db.fetch(query_string_completed)
    info[:number_of_accessions] = results.count
    info[:new] = results_new.count
    info[:in_progress] = results_in_progress.count
    info[:completed] = results_completed.count
    results
  end

  def record_type
    'accession'
  end

  def query_string
	  "select accession.id as id, 
	  	accession.identifier as accession_number,
		accession.title as accession_title,
	  	enumeration_value.value as processing_status,
		curator1.value as curator_1,
		curator2.value as curator_2,
		curator3.value as curator_3,
        user_defined.string_1 as resource_identifier
	from accession left join collection_management on accession.id = collection_management.accession_id 
	  	left join enumeration_value on collection_management.processing_status_id = enumeration_value.id
	        left join user_defined on accession.id = user_defined.accession_id	
		left join enumeration_value curator1 on curator1.id = user_defined.enum_2_id
		left join enumeration_value curator2 on curator2.id = user_defined.enum_3_id
		left join enumeration_value curator3 on curator3.id = user_defined.enum_4_id
	  order by enumeration_value.value"
  end
  
  def query_string_new
	  "select collection_management.id
	  	from collection_management left join enumeration_value on collection_management.processing_status_id = enumeration_value.id 
	  where enumeration_value.value = 'new' and collection_management.accession_id is not null"
  end

  def query_string_in_progress
	  "select collection_management.id
	  	from collection_management left join enumeration_value on collection_management.processing_status_id = enumeration_value.id 
	  where enumeration_value.value = 'In Progress' and collection_management.accession_id is not null"
  end

  def query_string_completed
	  "select collection_management.id
	  	from collection_management left join enumeration_value on collection_management.processing_status_id = enumeration_value.id 
	  where enumeration_value.value = 'Completed' and collection_management.accession_id is not null"
  end

  def clean_row(row)
    ReportUtils.fix_identifier_format(row, :accession_number)
  end

  def after_tasks
    info.delete(:repository)
  end

  def identifier_field
    :accession_number
  end

  def page_break
    false
  end
end
